using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Clerisy.CS.VirtualCashCard.Representations;
using Clerisy.CS.VirtualCashCard.Services;
using FluentAssertions;
using Xunit;

namespace Clerisy.CS.VirtualCashCard.Web.Test
{
    public class AccountServiceTest
    {
        private const string Pin = "1234";
        private const string WrongPin = "4321";
        private const string BadAccountNumber = "12345678";
        private string accountNumber;
        private AccountService subject;

        public AccountServiceTest()
        {
            subject = new AccountService();
            accountNumber = subject.Register(new CashCard(Pin));
            subject.Credit(new Transaction(accountNumber, 500m));
        }

        [Fact]
        public void ShouldRegisterNewCardAndReturnAccountNumber()
        {
            accountNumber.Should().Be("00000001");
        }

        [Fact]
        public void ShouldCreateSequentialAccountNumber()
        {
            accountNumber = subject.Register(new CashCard(Pin));
            accountNumber.Should().Be("00000002");
        }
        
        [Fact]
        public void ShouldCreditAmountToBalance()
        {
            var balance = subject.Credit(new Transaction(accountNumber, 30m));
            balance.Should().Be(530m);
        }
        
        [Fact]
        public void ShouldThrowOnCreditWhenAccountNumberNotFound()
        {
            Exception ex = Assert.Throws<ArgumentException>( 
                () =>  subject.Credit(new Transaction(BadAccountNumber, 30m)));

            ex.Message.Should().Be(AccountService.IncorrectAccountNumber);
        }
        
        [Fact]
        public void ShouldAllowValidWithdrawalWithValidPin()
        {
            var balance = subject.Withdraw(new Transaction(accountNumber, 30m), Pin);
            balance.Should().Be(470m);
        }
        
        [Fact]
        public void ShouldThrowExceptionWhenPinIncorrect()
        {
           Exception ex = Assert.Throws<ArgumentException>( 
                () =>  subject.Withdraw(new Transaction(accountNumber, 30m), WrongPin));

           ex.Message.Should().Be(AccountService.IncorrectPin);

        }
        
        [Fact]
        public void ShouldThrowExceptionWhenAccountNumberIncorrect()
        {
            Exception ex = Assert.Throws<ArgumentException>( 
                () =>  subject.Withdraw(new Transaction(BadAccountNumber, 30m), Pin));

            ex.Message.Should().Be(AccountService.IncorrectAccountNumber);

        }
        
        [Fact]
        public void ShouldThrowExceptionWhenInsufficientFunds()
        {
            Exception ex = Assert.Throws<ArgumentException>( 
                () =>  subject.Withdraw(new Transaction(accountNumber, 501m), Pin));

            ex.Message.Should().Be(AccountService.InsufficientFunds);

        }
        
        [Fact]
        public void ShouldSupportConcurrentAccessOnWithdrawal()
        {
            List<Task<decimal>> tasks = new List<Task<decimal>>();

            Func<decimal> withdrawal = () => subject.Withdraw(new Transaction(accountNumber, 100m), Pin);

            tasks.Add(Task.Factory.StartNew<decimal>(withdrawal));
            tasks.Add(Task.Factory.StartNew<decimal>(withdrawal));
            tasks.Add(Task.Factory.StartNew<decimal>(withdrawal));


            Task.WaitAll(tasks.ToArray());

            var decimals = tasks.Select(t => t.Result).ToList();
            decimals.Should().Contain(400m);
            decimals.Should().Contain(300m);
            decimals.Should().Contain(200m);
        }




    }
}