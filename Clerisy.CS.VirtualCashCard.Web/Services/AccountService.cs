using System;
using System.Collections.Generic;
using Clerisy.CS.VirtualCashCard.Representations;

namespace Clerisy.CS.VirtualCashCard.Services
{
    public class AccountService
    {
        private readonly Dictionary<string, CashCard> store = new Dictionary<string, CashCard>();
        private int accountSeed = 0;
        private readonly object locker = new object();

        public const string IncorrectAccountNumber = "Incorrect account number";
        public const string IncorrectPin = "Incorrect PIN";
        public const string InsufficientFunds = "Insufficient funds";


        public string Register(CashCard card)
        {
            accountSeed++;
            var accountNumber = accountSeed.ToString().PadLeft(8, '0');
            store.Add(accountNumber, card);
            return accountNumber;
        }

        public decimal Credit(Transaction credit)
        {
            
            if (!store.ContainsKey(credit.AccountNumber))
            {
                throw new ArgumentException(IncorrectAccountNumber);
            }

            lock (locker)
            {
                var cashCard = store[credit.AccountNumber];
                cashCard.Balance += credit.Amount;
                return cashCard.Balance;
            }
        }

        public decimal Withdraw(Transaction withdrawal, String pin)
        {
            if (!store.ContainsKey(withdrawal.AccountNumber))
            {
                throw new ArgumentException(IncorrectAccountNumber);
            }

            var card = store[withdrawal.AccountNumber];
            if (card.PINHashed != pin.GetHashCode())
            {
                throw new ArgumentException(IncorrectPin);
            }

            lock (locker)
            {
                if (card.Balance < withdrawal.Amount)
                {
                    throw new ArgumentException(InsufficientFunds);
                }

                card.Balance -= withdrawal.Amount;
                return card.Balance;
            }
        }
    }
}