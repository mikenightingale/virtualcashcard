using System;

namespace Clerisy.CS.VirtualCashCard.Representations
{
    public class Transaction
    {
        public decimal Amount { get; private set; }
        public string AccountNumber { get; private set; }

        public Transaction(string accountNumber, decimal amount)
        {   
            AccountNumber = accountNumber;
            Amount = amount;
        }
    }
}