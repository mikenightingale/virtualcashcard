using System;
using System.Net.Sockets;

namespace Clerisy.CS.VirtualCashCard.Representations
{
    public class CashCard
    {
        private Guid Id;
        public int PINHashed { get;}
        public decimal Balance { get; set; }
      

        public CashCard(string pin)
        {
            Id = Guid.NewGuid();
            PINHashed = pin.GetHashCode();
        }
    }
}